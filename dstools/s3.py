import pyarrow.parquet as pq
import pyarrow as pa
import pandas as pd
import warnings

def load_dataset_from_catalog(s3, client, db_name, table_name, index=None,
                              clean_ids=True, columns=None, **read_kwargs):
    location = get_catalog_location(client, db_name, table_name)
    table_resp = client.get_table(DatabaseName=db_name, Name=table_name)
    if table_resp["Table"]["StorageDescriptor"]["SerdeInfo"]["Name"] == "csv":
        return load_csv_dataset(s3, location, index, clean_ids, columns, **read_kwargs)
    else:
        return load_parquet_dataset(s3, location, index, clean_ids, columns, **read_kwargs)


def load_csv_dataset(s3, path, index=None, clean_ids=True, columns=None):
    urls = clean_paths(path)
    dfs = []
    for url in urls:
        if not url.endswith(".csv"):
            if not url.endswith("/"):
                url += "/"
            url = f"{url}data.csv"
        with s3.open(url) as csv:
            if columns:
                df = pd.read_csv(csv, usecols=columns)  # TODO: test
            else:
                df = pd.read_csv(csv)
            df = prepare_dataframe(df, index, clean_ids)
            dfs.append(df)
    df = pd.concat(dfs)
    del(dfs)
    return df


def load_dataset(s3, path, index=None, clean_ids=True):
    warnings.warn("load_dataset is deprecated. Use load_parquet_dataset.")
    return load_parquet_dataset(s3, path, index, clean_ids)


def clean_paths(path):
    urls = []
    if not isinstance(path, list):
        path = [path]
    for p in path:
        url = "s3://ucboitodaprd-oda-prod-etl-zepl/"
        if "s3://" in p:
            url = p
        else:
            url = f"{url}{p}"
        urls.append(url)
    return urls


def load_parquet_dataset(s3, path, index=None, clean_ids=True, columns=None, **parquet_kwargs):
    """Load a dataset from CU S3 buckets into a dataframe.

    s3: S3FileSystem instance for PyArrow to use.
    path: S3 path for the desired file. If s3:// is not included in the path then it is assumed
          to be in the zepl bucket for the time being.
    index: Column to use as the index. Optional.
    clean_ids: Option to set any column ending in _id or _sid to an int64
    """
    urls = clean_paths(path)
    if len(urls) > 1:
        dfs = []
        for u in urls:
            try:
                df = pq.ParquetDataset(u, filesystem=s3, **parquet_kwargs).read_pandas(columns=columns).to_pandas()
            except OSError as e:
                warnings.warn(e)
            dfs.append(df)
        df = pd.concat(dfs)
        del dfs
    else:
        try:
            df = pq.ParquetDataset(urls, filesystem=s3, **parquet_kwargs).read_pandas(columns=columns).to_pandas()
        except ValueError as e:
            if str(e).startswith("Schema in"):
                parts = s3.ls(urls[0])
                dfs = []
                for f in parts:
                    df = pq.ParquetDataset(f, filesystem=s3, **parquet_kwargs).read_pandas(columns=columns).to_pandas()
                    dfs.append(df)
                df = pd.concat(dfs)
                del dfs
    df = prepare_dataframe(df, index, clean_ids)
    return df


def prepare_dataframe(df, index=None, clean_ids=True):
    df.columns = [c.lower() for c in df.columns]
    id_cols = [c for c in df.columns if c.endswith("_id") or c.endswith("_sid")]
    if clean_ids:
        df.loc[:, id_cols] = df.loc[:, id_cols].astype("int64")
    if index is not None:
        df = df.set_index(index)
    return df


def save_dataset(s3, df, path):
    table = pa.Table.from_pandas(df)
    pq.write_table(table, f"s3://ucboitodaprd-oda-prod-etl-zepl/{path}", filesystem=s3, flavor="spark")


def get_catalog_location(client, db_name, table_name):
    """Return the location(s) of the most recent parquet file for the desired datasource
    using the boto3 glue client. Always returns a list of locations, even if it only
    has one item.

    client: authenticated boto3 glue client
    db_name: The name of the database in the glue catalog
    table_name: The name of the table in the glue catalog
    """
    locations = []
    table_resp = client.get_table(DatabaseName=db_name, Name=table_name)
    partitions_resp = client.get_partitions(DatabaseName=db_name, TableName=table_name)
    if len(table_resp["Table"]["PartitionKeys"]) != 0:
        partitions = partitions_resp.get("Partitions", [])
        locations = [p["StorageDescriptor"]["Location"] for p in partitions]
    else:
        locations.append(table_resp["Table"]["StorageDescriptor"].get("Location", None))
    return locations
